// Calculator
var print = document.getElementById('print');
var erase = false;

var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';
    erase = true;
  } else if (x === 'eval') {
        print.value = Math.round(evil(print.value) * 10000) / 10000;
        erase = true;
  } else if (x === 'log') {
        print.value = Math.log10(print.value);
  } else if (x === 'sin') {
        print.value = Math.sin(print.value * Math.PI / 180); //in degrees
        if(print.value == 1.2246467991473532e-16) print.value = 0;
  } else if (x === 'tan') {
        print.value = Math.tan(print.value * Math.PI / 180); //in degrees
        if(print.value == -1.2246467991473532e-16) print.value = 0;
        else if(print.value == 0.9999999999999999) print.value = 1;
  } else {
        print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}
// END

//Addition
var themes, object, themeObj;
themes = [
            {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
            {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
            {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
            {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
            {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
            {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
            {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
            {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
            {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
            {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
            {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
        ];
localStorage.setItem("themes", JSON.stringify(themes));

object = localStorage.getItem("themes");
$(document).ready(function() {
    $("#arrow").click(function() {
        var src = ($("#arrow").attr("src") === "https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png")
                  ? "https://png.icons8.com/collapse-arrow/win10/16/000000"
                  : "https://maxcdn.icons8.com/windows10/PNG/16/Arrows/angle_down-16.png";
        $("#arrow").attr("src", src);
        $(".chat-body").slideToggle("slow");
    });

    $("textarea").keypress(function(e){
        if (e.keyCode == 13 && !e.shiftKey)
        {        
            e.preventDefault();
            var txt = $("textarea").val();
            $(".msg-insert").append("<p class='msg-send'>"+txt+"</p>");
            $(".chat-body").animate({scrollTop: $(".chat-body")[0].scrollHeight}, 'fast'); //auto scroll to the latest chat
            $("textarea").val("");
            return;
        }
        else if (e.keyCode == 13 && e.shiftKey)
        {
            e.preventDefault();
            var txt = $("textarea").val();
            $(".msg-insert").append("<p class='msg-receive'>"+txt+"</p>");
            $(".chat-body").animate({scrollTop: $(".chat-body")[0].scrollHeight}, 'fast'); //auto scroll to the latest chat
            $("textarea").val("");
            return;
        }
    });
    $("#tutup-arrow").click(function(event){
        $("#atasnya-chat-body").hide();
        $("#tutup-arrow").hide();
        $("#buka-arrow").show();
  
    });
    $("#buka-arrow").click(function(event){

        $("#atasnya-chat-body").show();
        $("#tutup-arrow").show();
        $("#buka-arrow").hide();
      $("textarea").focus();        
    });


    $('.my-select').select2({
        'data' : JSON.parse(object)
    });

    //Retrieve applied theme when browser re-opened
    var applied = JSON.parse(localStorage.getItem("selected"));
    $("body").css({"background-color":applied.themeObj.bcgColor, "color":applied.themeObj.fontColor});    
});

$('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var data = JSON.parse(object);
    var id = $(".my-select").select2("val");
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
    $.each(data, function(i, theme) {
        if(id == theme.id) {
            // [TODO] ambil object theme yang dipilih
            themeObj = theme.text;
            // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
            $("body").css({"background-color" : theme.bcgColor, "color" : theme.fontColor});
            // [TODO] simpan object theme tadi ke local storage selectedTheme
            var selectedTheme = {themeObj:{"bcgColor":theme.bcgColor, "fontColor" : theme.fontColor}};
            localStorage.setItem("selected", JSON.stringify(selectedTheme));
            return false; //to break loop
        }
    });
});
//END
